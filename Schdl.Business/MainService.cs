﻿using System.Linq;
using System.Data.Entity;
using Schdl.Data.Contexts;
using Schdl.Domain.Entities.Schedule;
using Schdl.Math;

namespace Schdl.Business
{
    public class MainService : IMainService
    {
        private readonly ScheduleContext _context;
        private readonly IScheduleGenerator _schedule;

        public MainService(ScheduleContext context, IScheduleGenerator schedule)
        {
            _context = context;
            _schedule = schedule;
        }


        public void Run()
        {
            var teachers = _context.Teachers.Include(t => t.Subjects).ToList();

            var plans = _context.Plans
                .Include(t => t.Groups)
                .Include(t => t.Subjects)
                .ToList();

            var facilities = _context.Facilities.Include(f => f.Rooms).ToList();

            var schedules = _schedule.Generate(plans, facilities, teachers);

            _context.Set<Schedule>().AddRange(schedules);

            _context.SaveChanges();
        }
    }
}
