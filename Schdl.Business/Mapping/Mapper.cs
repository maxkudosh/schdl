﻿using Schdl.Business.Models;
using Schdl.Domain.Entities.Schedule;
using Schdl.Domain.Entities.University;

namespace Schdl.Business.Mapping
{
    public static class Mapper
    {
        public static void Initialize()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<GroupDto, Group>().ReverseMap();
                cfg.CreateMap<FacilityDto, Facility>().ReverseMap();
                cfg.CreateMap<PlanDto, Plan>().ReverseMap();
                cfg.CreateMap<PlanSubjectDto, PlanSubject>().ReverseMap();
                cfg.CreateMap<RoomDto, Room>().ReverseMap();
                cfg.CreateMap<ScheduleDto, Schedule>().ReverseMap();
                cfg.CreateMap<ScheduleEntryDto, ScheduleEntry>().ReverseMap();
                cfg.CreateMap<StudentDto, Student>().ReverseMap();
                cfg.CreateMap<SubjectDto, Subject>().ReverseMap();
                cfg.CreateMap<TeacherDto, Teacher>().ReverseMap();
            });
        }

        public static TTarget Map<TTarget, TSource>(TSource source)
        {
                return AutoMapper.Mapper.Map<TTarget>(source);
        }
    }
}
