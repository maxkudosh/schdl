﻿using System.Collections.Generic;

namespace Schdl.Business.Models
{
    public class FacilityDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public List<RoomDto> Rooms { get; set; }
    }
}
