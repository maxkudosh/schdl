﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Schdl.Business.Models
{
    public class GroupDto
    {
        public int Id { get; set; }
        public string Number { get; set; }
        [JsonProperty(ItemConverterType = typeof(IsoDateTimeConverter))]
        public DateTime EntryDate { get; set; }
        public PlanDto Plan { get; set; }
        public List<StudentDto> Students { get; set; }
    }
}
