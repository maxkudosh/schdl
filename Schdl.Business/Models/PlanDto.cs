﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Schdl.Business.Models
{
    public class PlanDto
    {
        public int PeriodInWeeks { get; set; }
        public int Id { get; set; }
        [JsonProperty(ItemConverterType = typeof(IsoDateTimeConverter))]
        public DateTime StartDate { get; set; }
        [JsonProperty(ItemConverterType = typeof(IsoDateTimeConverter))]
        public DateTime EndDate { get; set; }
        public List<PlanSubjectDto> Subjects { get; set; }
    }
}
