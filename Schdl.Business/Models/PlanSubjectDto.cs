﻿using Schdl.Domain.Enums;

namespace Schdl.Business.Models
{
    public class PlanSubjectDto
    {
        public int Id { get; set; }

        public ClassType Type { get; set; }
        public int Amount { get; set; }

        public SubjectDto Subject { get; set; }
    }
}
