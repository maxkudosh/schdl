﻿using Schdl.Domain.Enums;

namespace Schdl.Business.Models
{
    public class RoomDto
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public ClassType Type { get; set; }
        public int Capacity { get; set; }
    }
}
