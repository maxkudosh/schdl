﻿using System.Collections.Generic;
using Schdl.Domain.Enums;

namespace Schdl.Business.Models
{
    public class ScheduleDto
    {
        public int Id { get; set; }
        public GroupDto Group { get; set; }
        public List<ScheduleDayDto> Days { get; set; }
    }

    public class ScheduleDayDto
    {
        public int Number { get; set; }
        public List<ScheduleEntryDto> Classes { get; set; }
    }

    public class ScheduleEntryDto
    {
        public int Time { get; set; }
        public ClassType Type { get; set; }
        public SubjectDto Subject { get; set; }
        public TeacherDto Teacher { get; set; }
        public RoomDto Room { get; set; }
    }
}
