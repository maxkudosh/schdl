﻿namespace Schdl.Business.Models
{
    public class StudentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
