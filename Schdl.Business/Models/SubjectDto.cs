﻿using Schdl.Domain.Enums;

namespace Schdl.Business.Models
{
    public class SubjectDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Importance Importance { get; set; }
    }
}
