﻿using System.Collections.Generic;
using Schdl.Domain.Enums;

namespace Schdl.Business.Models
{
    public class TeacherDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TeacherQuality Quality { get; set; }
        public List<SubjectDto> Subjects { get; set; }
    }
}
