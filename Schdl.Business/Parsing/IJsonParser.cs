﻿using System.Threading.Tasks;

namespace Schdl.Business.Parsing
{
    public interface IJsonParser
    {
        Task<T> Parse<T>(string json);
    }
}
