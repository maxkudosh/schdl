﻿using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Schdl.Business.Parsing
{
    public class JsonParser: IJsonParser
    {
        public async Task<T> Parse<T>(string json)
        {
            return await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<T>(json));
        }
    }
}
