﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Schdl.Business.Mapping;
using Schdl.Data.Repositories;
using Schdl.Data.Units;
using Schdl.Domain.Entities;

namespace Schdl.Business.University
{
    public class CrudService<TDomain, TDto> : ICrudService<TDomain, TDto> where TDomain: Entity
    {
        protected readonly Repository<TDomain> Repository;

        public CrudService(IUnitOfWork uof)
        {
            Repository = uof.CreateRepository<TDomain>();
        }

        public async Task<List<TDto>> GetAll()
        {
            return (await Repository.GetAsync()).ToList().ConvertAll(Mapper.Map<TDto, TDomain>);
        }
    }
}
