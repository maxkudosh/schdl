﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Schdl.Business.Models;
using Schdl.Data.Units;
using Schdl.Domain.Entities.University;

namespace Schdl.Business.University
{
    public class FacilityService : IFacilityService
    {
        private readonly IUnitOfWork _uof;

        public FacilityService(IUnitOfWork uof)
        {
            _uof = uof;
        }

        public async Task AddRoomsToFacility(int facilityId, List<RoomDto> roomDtos)
        {
            var facilityRepository = _uof.CreateRepository<Facility>();
            var facility = facilityRepository.Find(facilityId);

            var roomRepository = _uof.CreateRepository<Room>();
            roomDtos.ForEach(r =>
            {
                var room = roomRepository.Find(r.Id);
                room.Facility = facility;
                roomRepository.Update(room);
            });

            await _uof.SaveAsync();
        }
    }
}
