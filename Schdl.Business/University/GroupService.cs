﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Schdl.Business.Mapping;
using Schdl.Business.Models;
using Schdl.Data.Units;
using Schdl.Domain.Entities.University;

namespace Schdl.Business.University
{
    public class GroupService : CrudService<Group, GroupDto>, IGroupService
    {
        private readonly IUnitOfWork _uof;

        public GroupService(IUnitOfWork uof) : base(uof)
        {
            _uof = uof;
        }

        public async Task AddStudentsToGroup(int groupId, List<StudentDto> studentDtos)
        {
            var group = Repository.Find(groupId);

            var studentRepository = _uof.CreateRepository<Student>();

            studentDtos.ForEach(s =>
            {
                var student = studentRepository.Find(s.Id);
                student.Group = group;
                studentRepository.Update(student);
            });

            await _uof.SaveAsync();
        }

        public async Task<List<GroupDto>> GetGroupsWithNoPlan()
        {
            var groups = await Repository.GetAsync(g => g.Plan == null);
            return groups.ToList().ConvertAll(Mapper.Map<GroupDto, Group>);
        }
    }
}
