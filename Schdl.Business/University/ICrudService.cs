﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Schdl.Business.University
{
    public interface ICrudService<TDomain,TDto>
    {
        Task<List<TDto>> GetAll();
    }
}