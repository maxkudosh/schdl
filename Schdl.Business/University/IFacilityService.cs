﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Schdl.Business.Models;

namespace Schdl.Business.University
{
    public interface IFacilityService
    {
        Task AddRoomsToFacility(int facilityId, List<RoomDto> roomDtos);
    }
}