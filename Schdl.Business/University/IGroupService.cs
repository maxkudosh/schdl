﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Schdl.Business.Models;

namespace Schdl.Business.University
{
    public interface IGroupService
    {
        Task AddStudentsToGroup(int groupId, List<StudentDto> studentDtos);
        Task<List<GroupDto>> GetGroupsWithNoPlan();
    }
}