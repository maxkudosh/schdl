﻿using System.Threading.Tasks;
using Schdl.Business.Models;

namespace Schdl.Business.University
{
    public interface IPlanService
    {
        Task AddPlanForGroup(int groupId, PlanDto planDto);
    }
}