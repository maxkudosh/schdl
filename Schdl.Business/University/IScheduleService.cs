﻿using System.Threading.Tasks;
using Schdl.Business.Models;

namespace Schdl.Business.University
{
    public interface IScheduleService
    {
        Task<ScheduleDto> GetSchedule(int groupId);
    }
}