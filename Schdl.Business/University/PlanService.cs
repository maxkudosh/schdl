﻿using System.Threading.Tasks;
using Schdl.Business.Models;
using Schdl.Data.Repositories;
using Schdl.Data.Units;
using Schdl.Domain.Entities.Schedule;
using Schdl.Domain.Entities.University;

namespace Schdl.Business.University
{
    public class PlanService : IPlanService
    {
        private readonly IUnitOfWork _uof;
        private readonly Repository<Plan> _repository;

        public PlanService(IUnitOfWork uof)
        {
            _uof = uof;
            _repository = uof.CreateRepository<Plan>();
        }

        public async Task AddPlanForGroup(int groupId, PlanDto planDto)
        {
            var plan = _repository.Find(planDto.Id);

            var groupRepository = _uof.CreateRepository<Group>();

            var group = groupRepository.Find(groupId);

            group.Plan = plan;

            await _uof.SaveAsync();
        }
    }
}
