﻿using System.Linq;
using System.Threading.Tasks;
using Schdl.Business.Mapping;
using Schdl.Business.Models;
using Schdl.Data.Repositories;
using Schdl.Data.Units;
using Schdl.Domain.Entities.Schedule;
using Schdl.Domain.Entities.University;

namespace Schdl.Business.University
{
    public class ScheduleService : IScheduleService
    {
        private readonly Repository<Schedule> _repository;

        public ScheduleService(IUnitOfWork uof)
        {
            _repository = uof.CreateRepository<Schedule>();
        }

        public async Task<ScheduleDto> GetSchedule(int groupId)
        {
            var schedule = (await _repository.GetAsync(s => s.Group.Id == groupId)).First();

            var scheduleByDay = schedule.Classes.GroupBy(c => c.Day).Select(g => new ScheduleDayDto
            {
                Number = g.Key,
                Classes = g.Select(i => new ScheduleEntryDto
                {
                    Subject = Mapper.Map<SubjectDto, Subject>(i.Subject),
                    Teacher = Mapper.Map<TeacherDto, Teacher>(i.Teacher),
                    Room = Mapper.Map<RoomDto, Room>(i.Room),
                    Time = (int) i.Time,
                    Type = i.Type
                }).ToList()
            }).ToList();

            var dto = new ScheduleDto
            {
                Id = schedule.Id,
                Group = Mapper.Map<GroupDto, Group>(schedule.Group),
                Days = scheduleByDay
            };

            return dto;
        }
    }
}
