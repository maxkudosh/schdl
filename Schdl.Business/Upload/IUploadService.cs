﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Schdl.Domain.Entities;

namespace Schdl.Business.Upload
{
    public interface IUploadService<TDomain, TDto> where TDomain : Entity
    {
        Task<TDto> Upload(string json);
        Task<List<TDto>> UploadMultiple(string json);
    }
}
