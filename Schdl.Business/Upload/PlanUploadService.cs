﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Schdl.Business.Models;
using Schdl.Business.Parsing;
using Schdl.Data.Repositories;
using Schdl.Data.Units;
using Schdl.Domain.Entities.Schedule;

namespace Schdl.Business.Upload
{
    public class PlanUploadService: UploadService<Plan, PlanDto>
    {
        private readonly Repository<Subject> _subjectRepository;

        public PlanUploadService(IUnitOfWork uof, IJsonParser json) : base(uof, json)
        {
            _subjectRepository = uof.CreateRepository<Subject>();
        }

        public override async Task<PlanDto> Upload(string json)
        {
            var plan = await ParseAndMap(json);

            foreach (var ps in plan.Subjects)
            {
                var subject = (await _subjectRepository.GetAsync(s => s.Name == ps.Subject.Name)).First();

                ps.Subject = subject;
            }

            return (await Save(new List<Plan> {plan})).First();
        }
    }
}
