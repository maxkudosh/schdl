﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Schdl.Business.Models;
using Schdl.Business.Parsing;
using Schdl.Data.Units;
using Schdl.Domain.Entities.Schedule;

namespace Schdl.Business.Upload
{
    public class SubjectUploadService: UploadService<Subject, SubjectDto>
    {

        public SubjectUploadService(IUnitOfWork uof, IJsonParser json) : base(uof, json)
        {
        }

        public override async Task<SubjectDto> Upload(string json)
        {
            var item = await ParseAndMap(json);

            if (IsNewSubject(item))
            {
                return (await Save(new List<Subject> {item})).First();
            }

            return null;
        }

        public override async Task<List<SubjectDto>> UploadMultiple(string json)
        {
            var items = await ParseAndMapMultiple(json);

            var itemsToAdd = items.FindAll(IsNewSubject);

            return await Save(itemsToAdd);
        }

        private bool IsNewSubject(Subject subject)
        {
            return Repository.FirstOrDefault(s => s.Name == subject.Name) == null;
        }
    }
}
