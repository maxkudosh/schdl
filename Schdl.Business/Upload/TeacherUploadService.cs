﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Schdl.Business.Models;
using Schdl.Business.Parsing;
using Schdl.Data.Units;
using Schdl.Domain.Entities.Schedule;

namespace Schdl.Business.Upload
{
    public class TeacherUploadService: UploadService<Teacher, TeacherDto>
    {
        public TeacherUploadService(IUnitOfWork uof, IJsonParser json) : base(uof, json)
        {
        }

        public override async Task<TeacherDto> Upload(string json)
        {
            var teacher = await ParseAndMap(json);

            var subjectRepository = Uof.CreateRepository<Subject>();

            var teacherSubjects = teacher.Subjects
                .Select(subject => subjectRepository.Get(s => s.Name == subject.Name).FirstOrDefault())
                .Where(subjectFromContext => subjectFromContext != null)
                .ToList();

            teacher.Subjects = teacherSubjects;

            return (await Save(new List<Teacher> {teacher})).First();
        }

        public override async Task<List<TeacherDto>> UploadMultiple(string json)
        {
            var teachers = await ParseAndMapMultiple(json);

            var subjectRepository = Uof.CreateRepository<Subject>();

            foreach (var teacher in teachers)
            {
                var teacherSubjects = teacher.Subjects
                    .Select(subject => subjectRepository.Get(s => s.Name == subject.Name).FirstOrDefault())
                    .Where(subjectFromContext => subjectFromContext != null)
                    .ToList();

                teacher.Subjects = teacherSubjects;
            }

            return await Save(teachers);
        }
    }
}
