﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Schdl.Business.Mapping;
using Schdl.Business.Parsing;
using Schdl.Data.Repositories;
using Schdl.Data.Units;
using Schdl.Domain.Entities;

namespace Schdl.Business.Upload
{
    public class UploadService<TDomain, TDto> : IUploadService<TDomain, TDto> where TDomain : Entity
    {
        protected readonly IUnitOfWork Uof;
        protected readonly IJsonParser Json;

        protected readonly Repository<TDomain> Repository;

        public UploadService(IUnitOfWork uof, IJsonParser json)
        {
            Uof = uof;
            Json = json;

            Repository = Uof.CreateRepository<TDomain>();
        }

        public virtual async Task<TDto> Upload(string json)
        {
            var item = await ParseAndMap(json);
            return (await Save(new List<TDomain> { item })).First();
        }

        public virtual async Task<List<TDto>> UploadMultiple(string json)
        {
            return await Save(await ParseAndMapMultiple(json));
        }

        protected async Task<List<TDto>> Save(List<TDomain> items)
        {
            items.ForEach(Repository.Insert);

            await Uof.SaveAsync();

            return items.ConvertAll(Mapper.Map<TDto, TDomain>);
        }

        protected async Task<TDomain> ParseAndMap(string json)
        {
            return Mapper.Map<TDomain, TDto>(await Json.Parse<TDto>(json));
        }

        protected async Task<List<TDomain>> ParseAndMapMultiple(string json)
        {
            var dtos = await Json.Parse<List<TDto>>(json);

            return dtos.ConvertAll(Mapper.Map<TDomain, TDto>);
        }
    }
}
