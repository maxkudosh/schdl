﻿using Microsoft.AspNet.Identity.EntityFramework;
using Schdl.Domain.Entities.Auth;

namespace Schdl.Data.Contexts
{
    public class AuthenticationContext: IdentityDbContext<ApplicationUser>
    {
        public AuthenticationContext(): base("DefaultConnection", false)
        {
        }

        public static AuthenticationContext Create()
        {
            return new AuthenticationContext();
        }
    }
}
