﻿using System.Data.Entity;
using Schdl.Domain.Entities.Schedule;
using Schdl.Domain.Entities.University;

namespace Schdl.Data.Contexts
{
    public class ScheduleContext: DbContext
    {
        public ScheduleContext() : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>().HasOptional(p => p.Plan).WithMany(g => g.Groups).Map(a => a.MapKey("PlanId"));
            modelBuilder.Entity<Schedule>().HasRequired(p => p.Group).WithOptional(g => g.Schedule).Map(a => a.MapKey("GroupId"));
        }

        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<PlanSubject> PlanSubjects { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<ScheduleEntry> ScheduleEntries { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
    }
}
