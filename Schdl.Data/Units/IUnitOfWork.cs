﻿using System.Threading.Tasks;
using Schdl.Data.Repositories;
using Schdl.Domain.Entities;

namespace Schdl.Data.Units
{
    public interface IUnitOfWork
    {
        Repository<T> CreateRepository<T>() where T : Entity;
        void Dispose();
        Task SaveAsync();
    }
}