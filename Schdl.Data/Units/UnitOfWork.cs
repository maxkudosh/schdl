﻿using System;
using System.Data.Entity.Validation;
using System.Threading.Tasks;
using Schdl.Data.Contexts;
using Schdl.Data.Repositories;
using Schdl.Domain.Entities;

namespace Schdl.Data.Units
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;

        private readonly ScheduleContext _scheduleContext;

        public UnitOfWork(ScheduleContext scheduleContext)
        {
            _scheduleContext = scheduleContext;
        }

        public Repository<T> CreateRepository<T>() where T : Entity
        {
            return new Repository<T>(_scheduleContext);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task SaveAsync()
        {
            try
            {
                await _scheduleContext.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _scheduleContext.Dispose();
            }
            _disposed = true;
        }
    }
}
