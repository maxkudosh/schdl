﻿using System;
using System.Collections.Generic;
using Schdl.Domain.Entities.University;

namespace Schdl.Domain.Entities.Schedule
{
    public class Plan: Entity
    {
        public int PeriodInWeeks { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual List<Group> Groups { get; set; }
        public virtual List<PlanSubject> Subjects { get; set; }

        public int GetNumberOfPeriods()
        {
            return (int) Math.Ceiling((EndDate - StartDate).TotalDays/(7d * PeriodInWeeks));
        }
    }
}
