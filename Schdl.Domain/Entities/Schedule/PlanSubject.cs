﻿using Schdl.Domain.Enums;

namespace Schdl.Domain.Entities.Schedule
{
    public class PlanSubject: Entity
    {
        public ClassType Type { get; set; }
        public int Amount { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Plan Plan { get; set; }
    }
}
