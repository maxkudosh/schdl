﻿using System.Collections.Generic;
using Schdl.Domain.Entities.University;

namespace Schdl.Domain.Entities.Schedule
{
    public class Schedule: Entity
    {
        public virtual Group Group { get; set; }
        public virtual List<ScheduleEntry> Classes { get; set; }
    }
}
