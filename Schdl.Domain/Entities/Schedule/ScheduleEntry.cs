﻿using Schdl.Domain.Entities.University;
using Schdl.Domain.Enums;

namespace Schdl.Domain.Entities.Schedule
{
    public class ScheduleEntry: Entity
    {
        public ClassType Type { get; set; }
        public ClassTime Time { get; set; }
        public int Day { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Room Room { get; set; }
        public virtual Teacher Teacher { get; set; }
    }
}
