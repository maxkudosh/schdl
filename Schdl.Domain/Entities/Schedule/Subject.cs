﻿using System;
using System.Collections.Generic;
using Schdl.Domain.Enums;

namespace Schdl.Domain.Entities.Schedule
{
    public class Subject: Entity, IEquatable<Subject>
    {
        public string Name { get; set; }
        public Importance Importance { get; set; }
        public virtual List<Teacher> Teachers { get; set; }

        public bool Equals(Subject other)
        {
            return Name == other.Name;
        }
    }
}
