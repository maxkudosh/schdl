﻿using System.Collections.Generic;
using Schdl.Domain.Enums;

namespace Schdl.Domain.Entities.Schedule
{
    public class Teacher: Entity
    {
        public string Name { get; set; }
        public TeacherQuality Quality { get; set; }
        public virtual List<Subject> Subjects { get; set; }
        public virtual List<TeacherAvailability> Availability { get; set; }
    }
}
