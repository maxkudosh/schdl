﻿using Schdl.Domain.Enums;

namespace Schdl.Domain.Entities.Schedule
{
    public class TeacherAvailability : Entity
    {
        public ClassDay Day { get; set; }
        public ClassTime Time { get; set; }
        public virtual Teacher Teacher { get; set; }
    }
}
