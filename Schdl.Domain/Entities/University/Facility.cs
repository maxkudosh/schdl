﻿using System.Collections.Generic;

namespace Schdl.Domain.Entities.University
{
    public class Facility: Entity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public virtual List<Room> Rooms { get; set; }
    }
}
