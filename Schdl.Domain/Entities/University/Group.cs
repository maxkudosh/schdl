﻿using System;
using System.Collections.Generic;
using Schdl.Domain.Entities.Schedule;

namespace Schdl.Domain.Entities.University
{
    public class Group: Entity
    {
        public string Number { get; set; }
        public DateTime EntryDate { get; set; }
        public virtual Plan Plan { get; set; }
        public virtual Schedule.Schedule Schedule { get; set; }
        public virtual List<Student> Students { get; set; }
    }
}
