﻿using Schdl.Domain.Enums;

namespace Schdl.Domain.Entities.University
{
    public class Room: Entity
    {
        public string Number { get; set; }
        public ClassType Type { get; set; }
        public int Capacity { get; set; }
        public virtual Facility Facility { get; set; }
    }
}
