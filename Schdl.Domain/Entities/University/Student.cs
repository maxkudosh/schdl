﻿namespace Schdl.Domain.Entities.University
{
    public class Student: Entity
    {
        public string Name { get; set; }
        public virtual Group Group { get; set; }
    }
}
