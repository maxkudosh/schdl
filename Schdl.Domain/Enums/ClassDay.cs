﻿namespace Schdl.Domain.Enums
{
    public enum ClassDay
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    }
}
