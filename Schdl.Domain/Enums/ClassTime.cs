﻿namespace Schdl.Domain.Enums
{
    public enum ClassTime
    {
        First,
        Second,
        Third,
        Fourth,
        Fifth,
        Sixth,
        Seventh,
        Eighth
    }
}
