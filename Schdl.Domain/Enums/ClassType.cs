﻿namespace Schdl.Domain.Enums
{
    public enum ClassType
    {
        Practice,
        Lab,
        Lecture
    }
}
