﻿namespace Schdl.Domain.Enums
{
    public enum Importance
    {
        High,
        Medium,
        Low
    }
}
