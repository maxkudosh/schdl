﻿namespace Schdl.Domain.Enums
{
    public enum RoomType
    {
        Lecture,
        Practice,
        Lab
    }
}
