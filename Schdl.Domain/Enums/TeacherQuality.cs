﻿namespace Schdl.Domain.Enums
{
    public enum TeacherQuality
    {
        None,
        Candidate,
        Doctor
    }
}
