﻿using Schdl.Domain.Entities.Schedule;

namespace Schdl.Math.Classes
{
    internal class BusyTeacher
    {
        public Teacher Teacher { get; set; }
        public int Business { get; set; }

        public BusyTeacher(Teacher teacher)
        {
            Teacher = teacher;
        }
    }
}
