﻿using System.Linq;
using Schdl.Domain.Entities.Schedule;
using Schdl.Domain.Entities.University;
using Schdl.Domain.Enums;

namespace Schdl.Math.Classes
{
    internal class Class
    {
        public Class(Group @group, Teacher teacher, Subject subject, ClassType type, Plan plan)
        {
            Group = @group;
            Teacher = teacher;
            Subject = subject;
            Type = type;
            Plan = plan;
        }

        public Plan Plan { get; }
        public Group Group { get; }
        public Teacher Teacher { get; }
        public Subject Subject { get; }
        public ClassType Type { get; }

        public int GetRequiredCapacity()
        {
            return Group?.Students.Count ?? Plan.Groups.Select(g => g.Students.Count).Aggregate((a, b) => a + b);
        }

        public override string ToString()
        {
            return $"{Group.Number} - {Teacher.Name} - {Subject.Name} - {Type}";
        }
    }
}
