﻿using System.Collections.Generic;
using System.Linq;
using Schdl.Domain.Entities.Schedule;
using Schdl.Domain.Entities.University;

namespace Schdl.Math.Classes
{
    internal class Classes
    {
        private readonly List<Plan> _plans;
        private readonly List<Facility> _facilities;
        private readonly Dictionary<int, LinkedList<BusyTeacher>> _teachersLookup;

        public Classes(List<Plan> plans, List<Facility> facilities, IEnumerable<Teacher> teachers)
        {
            _plans = plans;
            _facilities = facilities;
            _teachersLookup = InitializeTeachersLookup(teachers);
        }

        public IOrderedEnumerable<WeighedClass> GetClasses()
        {
            var classes = GenerateClassesList();
            var weighedClasses = AddWeightsToClasses(classes);
            return OrderClasses(weighedClasses);
        }

        private List<Class> GenerateClassesList()
        {
            var subjects = _plans.SelectMany(p => p.Subjects)
                .OrderByDescending(s => s.Subject.Importance)
                .ThenByDescending(s => s.Type)
                .ToList();

            var periodsInPlan = _plans[0].GetNumberOfPeriods();

            var classes = new List<Class>(subjects.Count);

            foreach (var s in subjects)
            {
                var subjectTeachers = _teachersLookup[s.Subject.Id];

                var leastBusyTeacher = subjectTeachers.First.Value;

                var classHoursInPeriod = (int)System.Math.Ceiling(s.Amount / (double)periodsInPlan);

                foreach (var group in s.Plan.Groups)
                {
                    for (var i = 0; i < classHoursInPeriod; i++)
                    {
                        classes.Add(new Class(group, leastBusyTeacher.Teacher, s.Subject, s.Type, s.Plan));
                    }
                }

                leastBusyTeacher.Business += classHoursInPeriod * s.Plan.Groups.Count;
                subjectTeachers.RemoveFirst();
                subjectTeachers.AddLast(leastBusyTeacher);
            }

            return classes;
        }

        private IEnumerable<WeighedClass> AddWeightsToClasses(IReadOnlyCollection<Class> classes)
        {
            var rooms = _facilities.SelectMany(f => f.Rooms).ToList();

            var weighedClasses = new List<WeighedClass>();

            foreach (var c in classes)
            {
                var requiredCapacity = c.GetRequiredCapacity();

                var availableRooms = rooms.Count(r => r.Type == c.Type && r.Capacity >= requiredCapacity);

                var teacherBusiness = _teachersLookup[c.Subject.Id].First(t => t.Teacher == c.Teacher).Business;

                var groupBusiness = classes.Count(cl => cl.Group == null || cl.Group == c.Group);

                var classWeight = teacherBusiness * groupBusiness / availableRooms;

                weighedClasses.Add(new WeighedClass(c, classWeight));
            }

            return weighedClasses;
        }

        private static IOrderedEnumerable<WeighedClass> OrderClasses(IEnumerable<WeighedClass> classes)
        {
            return classes.OrderByDescending(c => c.Weight);
        }

        private static Dictionary<int, LinkedList<BusyTeacher>> InitializeTeachersLookup(IEnumerable<Teacher> teachers)
        {
            var mappedTeachers = teachers.Select(t => new BusyTeacher(t)).ToList();

            return teachers
                .SelectMany(t => t.Subjects)
                .Distinct()
                .Select(s =>
                {
                    var subjectTeachers = mappedTeachers.FindAll(t => t.Teacher.Subjects.Contains(s));
                    var busyTeachers = new LinkedList<BusyTeacher>(subjectTeachers.OrderByDescending(t => t.Teacher.Quality));
                    return new KeyValuePair<Subject, LinkedList<BusyTeacher>>(s, busyTeachers);
                })
                .ToDictionary(i => i.Key.Id, i => i.Value);
        }
    }
}
