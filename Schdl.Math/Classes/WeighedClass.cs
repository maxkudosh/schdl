﻿namespace Schdl.Math.Classes
{
    internal class WeighedClass
    {
        public int Weight { get; private set; }
        public Class Class { get; private set; }

        public WeighedClass(Class @class, int weight)
        {
            Class = @class;
            Weight = weight;
        }
    }
}
