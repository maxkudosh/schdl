﻿using System.Collections.Generic;
using System.Linq;
using Schdl.Domain.Enums;
using Schdl.Math.Classes;

namespace Schdl.Math.Criterias
{
    internal class CreateGroupHole: Criteria
    {
        public CreateGroupHole(List<Reservation> schedule) : base(-10, schedule)
        {
        }

        protected override double CountCore(Class c, FundEntry e)
        {
            if ((e.Time == ClassTime.First || e.Time == ClassTime.Second) && CheckUp(c, e))
            {
                return 1;
            }

            if ((e.Time == ClassTime.Eighth || e.Time == ClassTime.Seventh) && CheckDown(c, e))
            {
                return 1;
            }

            if (CheckUp(c, e) || CheckDown(c, e))
            {
                return 1;
            }

            return 0;
        }

        private bool CheckUp(Class c, FundEntry e)
        {
            return !Schedule.Any(r => r.Class.Group == c.Group
                                      && r.Entry.DayInPeriod == e.DayInPeriod
                                      && (int) r.Entry.Time == (int) e.Time + 1)
                   && Schedule.Any(r => r.Class.Group == c.Group
                                        && r.Entry.DayInPeriod == e.DayInPeriod
                                        && (int) r.Entry.Time != (int) e.Time + 1
                                        && r.Entry.Time > e.Time);
        }

        private bool CheckDown(Class c, FundEntry e)
        {
            return !Schedule.Any(r => r.Class.Group == c.Group
                                      && r.Entry.DayInPeriod == e.DayInPeriod
                                      && (int)r.Entry.Time == (int)e.Time - 1)
                   && Schedule.Any(r => r.Class.Group == c.Group
                                        && r.Entry.DayInPeriod == e.DayInPeriod
                                        && (int)r.Entry.Time != (int)e.Time - 1
                                        && r.Entry.Time < e.Time);
        }
    }
}
