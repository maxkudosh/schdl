﻿using System.Collections.Generic;
using Schdl.Math.Classes;

namespace Schdl.Math.Criterias
{
    internal abstract class Criteria
    {
        protected readonly int Weight;
        protected List<Reservation> Schedule;

        protected Criteria(int weight, List<Reservation> schedule)
        {
            Weight = weight;
            Schedule = schedule;
        }


        public double Count(Class c, FundEntry e)
        {
            return Weight*CountCore(c, e);
        }

        protected abstract double CountCore(Class c, FundEntry e);
    }
}
