﻿using System.Collections.Generic;
using Schdl.Math.Classes;
using Schdl.Domain.Enums;

namespace Schdl.Math.Criterias
{
    internal class EarlyOnMondayOrSaturday: Criteria
    {
        public EarlyOnMondayOrSaturday(List<Reservation> schedule) : base(-10, schedule)
        {
        }

        protected override double CountCore(Class c, FundEntry e)
        {
            if (c.Subject.Importance == Importance.Low)
            {
                return 0;
            }

            if (c.Subject.Importance == Importance.Medium)
            {
                if (e.IsMonday() && e.Time == ClassTime.First)
                {
                    return 0.6;
                }

                if (e.IsSaturday() && e.Time == ClassTime.First)
                {
                    return 0.5;
                }
                return 0;
            }


            if(e.IsMonday() && e.Time == ClassTime.First)
            {
                return 1;
            }

            if (e.IsMonday() && e.Time == ClassTime.Second)
            {
                return 0.5;
            }

            if (e.IsSaturday())
            {
                return 1;
            }

            return 0;
        }
    }
}
