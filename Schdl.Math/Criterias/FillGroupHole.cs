﻿using System.Collections.Generic;
using System.Linq;
using Schdl.Math.Classes;
using Schdl.Domain.Enums;

namespace Schdl.Math.Criterias
{
    internal class FillGroupHole: Criteria
    {
        public FillGroupHole(List<Reservation> schedule) : base(10, schedule)
        {
        }

        protected override double CountCore(Class c, FundEntry e)
        {
            var thereIsNextClass =
                e.Time != ClassTime.Eighth
                && Schedule.Any(r => r.Entry.DayInPeriod == e.DayInPeriod
                                     && (int) r.Entry.Time == (int) e.Time + 1
                                     && e.Room == r.Entry.Room);

            var thereIsPreviousClass = e.Time != ClassTime.First
                                       && Schedule.Any(r => r.Entry.DayInPeriod == e.DayInPeriod
                                                            && (int) r.Entry.Time == (int) e.Time - 1
                                                            && e.Room == r.Entry.Room);


            if (thereIsNextClass && thereIsPreviousClass)
            {
                return 1;
            }

            return 0;
        }
    }
}
