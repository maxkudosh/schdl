﻿using System.Collections.Generic;
using System.Linq;
using Schdl.Math.Classes;

namespace Schdl.Math.Criterias
{
    internal class GroupNotBusy: Criteria
    {
        public GroupNotBusy(List<Reservation> schedule) : base(10, schedule)
        {
        }

        protected override double CountCore(Class c, FundEntry e)
        {
            double score = 0;

            var classesThatDay = Schedule.Count(r => r.Class.Group == c.Group && r.Entry.DayInPeriod == e.DayInPeriod);

            if (classesThatDay == 0)
            {
               score = 1;
            }

            if (classesThatDay == 1)
            {
                score = 0.8;
            }

            if (classesThatDay == 2)
            {
                score = 0.6;
            }

            if (classesThatDay == 3)
            {
                score = 0.5;
            }

            if (e.IsSaturday())
            {
                score = score / 3;
            }

            return score;
        }
    }
}
