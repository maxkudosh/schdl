﻿using System.Collections.Generic;
using System.Linq;
using Schdl.Math.Classes;

namespace Schdl.Math.Criterias
{
    internal class GroupOverload: Criteria
    {
        public GroupOverload(List<Reservation> schedule) : base(-10, schedule)
        {
        }

        protected override double CountCore(Class c, FundEntry e)
        {
            var classesThatDay = Schedule.Count(r => r.Class.Group == c.Group && r.Entry.DayInPeriod == e.DayInPeriod);

            if (classesThatDay == 4)
            {
                return 0.75;
            }

            if (classesThatDay >= 5)
            {
                return 1;
            }

            return 0;
        }
    }
}
