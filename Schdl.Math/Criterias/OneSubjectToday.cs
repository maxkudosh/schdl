﻿using System.Collections.Generic;
using System.Linq;
using Schdl.Math.Classes;

namespace Schdl.Math.Criterias
{
    internal class OneSubjectToday: Criteria
    {
        public OneSubjectToday(List<Reservation> schedule) : base(-10, schedule)
        {
        }

        protected override double CountCore(Class c, FundEntry e)
        {
            var sameSubjectWithSameTypeToday =
                Schedule.Any(
                    r => r.Class.Subject == c.Subject
                         && r.Class.Type == c.Type
                         && r.Entry.DayInPeriod == e.DayInPeriod);

            return sameSubjectWithSameTypeToday ? 1 : 0;
        }
    }
}
