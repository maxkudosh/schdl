﻿using System.Collections.Generic;
using Schdl.Math.Classes;

namespace Schdl.Math.Criterias
{
    internal class RoomSize: Criteria
    {
        public RoomSize(List<Reservation> schedule) : base(5, schedule)
        {
        }

        protected override double CountCore(Class c, FundEntry e)
        {
            var ratio = c.GetRequiredCapacity()/e.Room.Capacity;

            return ratio;
        }
    }
}
