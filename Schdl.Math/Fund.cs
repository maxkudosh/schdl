﻿using System;
using System.Collections.Generic;
using System.Linq;
using Schdl.Domain.Entities.University;
using Schdl.Domain.Enums;

namespace Schdl.Math
{
    internal class FundEntry: IEquatable<FundEntry>
    {
        public Room Room { get; }
        public ClassTime Time { get; }
        public int DayInPeriod { get;}

        public FundEntry(int dayInPeriod, ClassTime time, Room room)
        {
            DayInPeriod = dayInPeriod;
            Time = time;
            Room = room;
        }

        public override bool Equals(object obj)
        {
            var other = (FundEntry)obj;
            return Equals(other);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public bool Equals(FundEntry other)
        {
            return Time == other.Time && DayInPeriod == other.DayInPeriod && Room.Id == other.Room.Id;
        }

        public override string ToString()
        {
            return $"{Room.Number} - {DayInPeriod} - { Time}";
        }

        public bool IsMonday()
        {
            return DayInPeriod%6 == 0;
        }

        public bool IsSaturday()
        {
            return DayInPeriod % 6 == 5;
        }
    }

    internal class Fund
    {
        private readonly List<ClassTime> _timesInDay = new List<ClassTime>
        {
            ClassTime.First,
            ClassTime.Second,
            ClassTime.Third,
            ClassTime.Fourth,
            ClassTime.Fifth,
            ClassTime.Sixth,
            ClassTime.Seventh,
            ClassTime.Eighth
        };

        public HashSet<FundEntry> Lecture { get; private set; }
        public HashSet<FundEntry> Lab { get; private set; }
        public HashSet<FundEntry> Practice { get; private set; }

        public Fund(IEnumerable<Facility> facilities, int weeksInPeriod)
        {
            var rooms = facilities.SelectMany(f => f.Rooms).ToList();

            const int studyDaysInWeek = 6;

            var periodDays = weeksInPeriod*studyDaysInWeek;

            Lecture = CreateFund(ClassType.Lecture, rooms, periodDays);
            Lab = CreateFund(ClassType.Lab, rooms, periodDays);
            Practice = CreateFund(ClassType.Practice, rooms, periodDays);

        }

        private HashSet<FundEntry> CreateFund(ClassType type, List<Room> rooms, int periodDays)
        {
            var roomsOfType = rooms.FindAll(r => r.Type == type);
            var fund = new HashSet<FundEntry>();

            foreach (var r in roomsOfType)
            {
                foreach (var t in _timesInDay)
                {
                    for (var i = 0; i < periodDays; i++)
                    {
                        fund.Add(new FundEntry(i, t, r));
                    }
                }
            }

            return fund;
        }
    }
}
