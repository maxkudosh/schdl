﻿using System.Collections.Generic;
using Schdl.Domain.Entities.Schedule;
using Schdl.Domain.Entities.University;

namespace Schdl.Math
{
    public interface IScheduleGenerator
    {
        List<Schedule> Generate(List<Plan> plans, List<Facility> facilities, List<Teacher> teachers);
    }
}