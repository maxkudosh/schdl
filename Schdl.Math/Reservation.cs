﻿using Schdl.Math.Classes;

namespace Schdl.Math
{
    internal class Reservation
    {
        public double Weight { get; private set; }
        public Class Class { get; private set; }
        public FundEntry Entry { get; private set; }

        public Reservation(Class @class, FundEntry entry, double weight)
        {
            Class = @class;
            Entry = entry;
            Weight = weight;
        }

        public override string ToString()
        {
            return $"{Class} ! {Entry}";
        }
    }
}
