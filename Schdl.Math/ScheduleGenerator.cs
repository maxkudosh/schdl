﻿using System;
using System.Linq;
using System.Collections.Generic;
using Schdl.Domain.Entities.Schedule;
using Schdl.Domain.Entities.University;
using Schdl.Domain.Enums;
using Schdl.Math.Classes;
using Schdl.Math.Criterias;

namespace Schdl.Math
{
    public class ScheduleGenerator : IScheduleGenerator
    {
        private Fund _fund;
        private HashSet<FundEntry> _occupiedEntries;

        public List<Schedule> Generate(List<Plan> plans, List<Facility> facilities, List<Teacher> teachers)
        {
            var classesGenerator = new Classes.Classes(plans, facilities, teachers);
            var classes = classesGenerator.GetClasses();

            _fund = new Fund(facilities, 1);

            _occupiedEntries = new HashSet<FundEntry>();

            var reservations = new List<Reservation>();

            foreach (var c in classes)
            {
                var highestScore = double.MinValue;

                FundEntry bestEntry = null;

                foreach (var e in GetEntriesForType(c.Class.Type))
                {
                    if (!DoesFundEntryFitsClass(c.Class, e, reservations))
                    {
                        continue;
                    }

                    var score = CountClassWeightForFundEntry(c.Class, e, reservations);

                    if (score > highestScore)
                    {
                        highestScore = score;
                        bestEntry = e;
                    }
                }

                reservations.Add(new Reservation(c.Class, bestEntry, highestScore));

                _occupiedEntries.Add(bestEntry);
            }


            reservations = reservations.OrderBy(r => r.Entry.DayInPeriod).ThenBy(r => r.Entry.Time).ToList();
            _occupiedEntries = new HashSet<FundEntry>();

            foreach (var c in classes)
            {
                var existingReservation = reservations.Find(r => r.Class == c.Class);
                var highestScore = existingReservation.Weight;

                var bestEntry = existingReservation.Entry;

                foreach (var e in GetEntriesForType(c.Class.Type))
                {
                    if (!DoesFundEntryFitsClass(c.Class, e, reservations))
                    {
                        continue;
                    }

                    var score = CountClassWeightForFundEntry(c.Class, e, reservations);

                    if (score > highestScore)
                    {
                        highestScore = score;
                        bestEntry = e;
                    }
                }

                _occupiedEntries.Remove(existingReservation.Entry);
                reservations.Remove(existingReservation);

                var betterReservation = new Reservation(c.Class, bestEntry, highestScore);
                reservations.Add(betterReservation);
                _occupiedEntries.Add(bestEntry);
            }


            var timetable = reservations.OrderBy(r => r.Entry.DayInPeriod).ThenBy(r => r.Entry.Time).ToList();

            var groups = plans.SelectMany(p => p.Groups);

            var schedules = new List<Schedule>();

            foreach (var g in groups)
            {
                var groupClasses = timetable.FindAll(r => r.Class.Group == g);

                var schedule = new Schedule
                {
                    Group = g
                };

                var scheduleEntries = groupClasses.ConvertAll(c => new ScheduleEntry
                {
                    Type = c.Class.Type,
                    Time = c.Entry.Time,
                    Day = c.Entry.DayInPeriod,
                    Subject = c.Class.Subject,
                    Room = c.Entry.Room,
                    Teacher = c.Class.Teacher
                });

                schedule.Classes = scheduleEntries;

                schedules.Add(schedule);
            }


            return schedules;
        }

        private HashSet<FundEntry> GetEntriesForType(ClassType type)
        {
            switch (type)
            {
                case ClassType.Lecture:
                    return _fund.Lecture;

                case ClassType.Practice:
                    return _fund.Practice;

                case ClassType.Lab:
                    return _fund.Lab;

                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private bool DoesFundEntryFitsClass(Class c, FundEntry e, List<Reservation> schedule)
        {
            if (_occupiedEntries.Contains(e))
            {
                if (c.Type != ClassType.Lecture)
                {
                    return false;
                }

                var reservationForEntry = schedule.Find(r => r.Entry.Equals(e));

                if (!(reservationForEntry.Class.Plan == c.Plan && reservationForEntry.Class.Subject == c.Subject))
                {
                    return false;
                }
            }

            if (e.Room.Type != c.Type)
            {
                return false;
            }

            if (e.Room.Capacity < c.GetRequiredCapacity())
            {
                return false;
            }

            if (schedule.Find(r => r.Class.Group == c.Group && r.Entry.DayInPeriod == e.DayInPeriod && r.Entry.Time == e.Time) != null)
            {
                return false;
            }

            if (schedule.Find(r => r.Class.Teacher == c.Teacher && r.Entry.DayInPeriod == e.DayInPeriod && r.Entry.Time == e.Time) != null)
            {
                return false;
            }

            return true;
        }

        private double CountClassWeightForFundEntry(Class c, FundEntry e, List<Reservation> schedule)
        {
            var createGroupHole = new CreateGroupHole(schedule).Count(c, e);
            var early = new EarlyOnMondayOrSaturday(schedule).Count(c, e);
            var fillGroupHole = new FillGroupHole(schedule).Count(c, e);
            var groupNotBusy = new GroupNotBusy(schedule).Count(c, e);
            var groupOverload = new GroupOverload(schedule).Count(c, e);
            var roomSize = new RoomSize(schedule).Count(c, e);
            var sameSubjectToday = new OneSubjectToday(schedule).Count(c, e);

            return createGroupHole + early + fillGroupHole + groupNotBusy + groupOverload + roomSize + sameSubjectToday;
        }
    }
}
