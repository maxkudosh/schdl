﻿using System;
using Schdl.Business;
using Schdl.Math;

namespace Schdl.Runner
{
    class Program
    {
        static void Main()
        {
            var runner = new MainService(new Data.Contexts.ScheduleContext(), new ScheduleGenerator());
            runner.Run();
            Console.ReadLine();
        }
    }
}
