﻿using System.Web.Optimization;

namespace Schdl
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/scripts/vendors").Include(
                "~/public/vendors/jquery/jquery.js",
                "~/public/vendors/bootstrap/bootstrap.js",
                "~/public/vendors/angular/angular.js",
                "~/public/vendors/angular-bootstrap/bootstrap.js",
                "~/public/vendors/angular-cookies/cookies.js",
                "~/public/vendors/angular-resource/resource.js",
                "~/public/vendors/angular-router/router.js",
                "~/public/vendors/angular-file/upload.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/app").Include(
                "~/public/app/app.js",
                "~/public/app/app.controller.js",
                "~/public/app/components/routes/routes.js",
                "~/public/app/components/routes/upload-pills.js",
                "~/public/app/components/routes/core-pills.js",
                "~/public/app/components/sidebar/sidebar.js",
                "~/public/app/components/navbar/navbar.js",
                "~/public/app/components/navbar/navbar.controller.js",
                "~/public/app/components/navbar/navbar.directive.js",
                "~/public/app/components/upload/index.js",
                "~/public/app/components/upload/directive.js",
                "~/public/app/components/services/index.js",
                "~/public/app/components/select/index.js",
                "~/public/app/upload/upload.js",
                "~/public/app/upload/group/index.js",
                "~/public/app/upload/subject/index.js",
                "~/public/app/upload/teacher/index.js",
                "~/public/app/upload/facility/index.js",
                "~/public/app/upload/plan/index.js",
                "~/public/app/core/index.js",
                "~/public/app/core/group/index.js",
                "~/public/app/core/group/schedule/index.js",
                "~/public/app/core/group/plan/index.js",
                "~/public/app/core/group/students/index.js"
                ));

            bundles.Add(new StyleBundle("~/styles/vendors").Include(
                "~/public/vendors/bootstrap/bootstrap.css"
                ));

            bundles.Add(new StyleBundle("~/styles/app").Include(
                "~/public/app/components/upload/upload.css",
                "~/public/app/app.css"
                ));
        }
    }
}
