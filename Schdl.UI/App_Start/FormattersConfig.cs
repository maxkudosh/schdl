﻿using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace Schdl.App_Start
{
    public static class FormattersConfig
    {
        public static void Initialize(HttpConfiguration config)
        {
            var jsonFormatter = config.Formatters.JsonFormatter;

            jsonFormatter.SerializerSettings.ContractResolver =
                new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.Converters.Add
                (new Newtonsoft.Json.Converters.StringEnumConverter());
        }
    }
}