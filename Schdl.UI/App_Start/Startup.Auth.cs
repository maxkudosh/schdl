﻿using Microsoft.Owin.Security.Cookies;
using Owin;
using Schdl.Data.Contexts;

namespace Schdl
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(AuthenticationContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions());
        }
    }
}
