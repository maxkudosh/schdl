﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.University;

namespace Schdl.Controllers.Api
{
    public class CrudController<TDomain, TDto>: ApiController
    {
        protected readonly ICrudService<TDomain, TDto> Crud;

        public CrudController(ICrudService<TDomain, TDto> crud)
        {
            Crud = crud;
        }

        public async Task<List<TDto>> GetAll()
        {
            return await Crud.GetAll();
        }

        public async Task<TDto> Get(int id)
        {
            return await Crud.Get(id);
        }
    }
}