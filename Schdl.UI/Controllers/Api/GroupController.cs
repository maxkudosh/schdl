﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business;
using Schdl.Business.Models;
using Schdl.Business.University;
using Schdl.Domain.Entities.University;

namespace Schdl.Controllers.Api
{
    public class GroupApiController: CrudController<Group, GroupDto>
    {
        private readonly IGroupService _groupService;
        private readonly IMainService _mainService;

        public GroupApiController(IGroupService groupService, ICrudService<Group, GroupDto> crud, IMainService mainService) : base(crud)
        {
            _groupService = groupService;
            _mainService = mainService;
        }

        [HttpGet]
        [Route("api/group")]
        public async Task<List<GroupDto>> GetGroups()
        {
            //_mainService.Run();
            return await GetAll();
        }

        [HttpGet]
        [Route("api/group/single/{groupId}")]
        public async Task<GroupDto> GetGroup(int groupId)
        {
            return await Get(groupId);
        }

        [HttpGet]
        [Route("api/group/noplan")]
        public async Task<List<GroupDto>> GetGroupsWithNoPlan()
        {
            return await _groupService.GetGroupsWithNoPlan();
        }
    }
}