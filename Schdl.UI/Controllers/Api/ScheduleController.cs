﻿using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Models;
using Schdl.Business.University;

namespace Schdl.Controllers.Api
{
    public class ScheduleController : ApiController
    {
        private readonly IScheduleService _scheduleService;

        public ScheduleController(IScheduleService scheduleService)
        {
            _scheduleService = scheduleService;
        }

        [HttpGet]
        [Route("api/schedule/{groupId}")]
        public async Task<ScheduleDto> GetSchedule(int groupId)
        {
            return await _scheduleService.GetSchedule(groupId);
        }
    }
}