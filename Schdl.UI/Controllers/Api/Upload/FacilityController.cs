﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Models;
using Schdl.Business.Upload;
using Schdl.Domain.Entities.University;

namespace Schdl.Controllers.Api.Upload
{
    public class FacilityController: UploadController<Facility, FacilityDto>
    {
        public FacilityController(IUploadService<Facility, FacilityDto> uploadService): base(uploadService)
        {
        }

        [HttpPost]
        [Route("api/facility/upload")]
        public async Task<List<FacilityDto>> UploadFacilities()
        {
            return await UploadMultiple();
        }
    }
}