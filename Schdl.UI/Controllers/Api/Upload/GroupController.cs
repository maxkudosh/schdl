﻿using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Models;
using Schdl.Business.Upload;
using Schdl.Domain.Entities.University;

namespace Schdl.Controllers.Api.Upload
{
    public class GroupController: UploadController<Group, GroupDto>
    {
        public GroupController(IUploadService<Group, GroupDto> uploadService): base(uploadService)
        {
        }

        [HttpPost]
        [Route("api/group/upload")]
        public async Task<GroupDto> UploadGroups()
        {
            return await Upload();
        }
    }
}