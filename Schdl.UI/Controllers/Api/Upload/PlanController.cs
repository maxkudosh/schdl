﻿using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Models;
using Schdl.Business.University;
using Schdl.Business.Upload;
using Schdl.Domain.Entities.Schedule;

namespace Schdl.Controllers.Api.Upload
{
    public class PlanController: UploadController<Plan, PlanDto>
    {
        private readonly IPlanService _planService;

        public PlanController(IUploadService<Plan, PlanDto> uploadService, IPlanService planService): base(uploadService)
        {
            _planService = planService;
        }

        [HttpPost]
        [Route("api/plan/upload/{groupId}")]
        public async Task<PlanDto> UploadPlans(int groupId)
        {
            var dto = await Upload();

            await _planService.AddPlanForGroup(groupId, dto);

            return dto;
        }
    }
}