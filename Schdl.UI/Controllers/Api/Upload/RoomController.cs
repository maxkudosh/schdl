﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Models;
using Schdl.Business.University;
using Schdl.Business.Upload;
using Schdl.Domain.Entities.University;

namespace Schdl.Controllers.Api.Upload
{
    public class RoomController: UploadController<Room, RoomDto>
    {
        private readonly IFacilityService _facilityService;

        public RoomController(IUploadService<Room, RoomDto> uploadService, IFacilityService facilityService): base(uploadService)
        {
            _facilityService = facilityService;
        }

        [HttpPost]
        [Route("api/room/upload/{facilityId}")]
        public async Task<List<RoomDto>> UploadRooms(int facilityId)
        {
            var uploadedRooms = await UploadMultiple();

            await _facilityService.AddRoomsToFacility(facilityId, uploadedRooms);

            return uploadedRooms;
        }
    }
}