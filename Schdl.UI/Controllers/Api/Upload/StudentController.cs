﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Models;
using Schdl.Business.University;
using Schdl.Business.Upload;
using Schdl.Domain.Entities.University;

namespace Schdl.Controllers.Api.Upload
{
    public class StudentController: UploadController<Student, StudentDto>
    {
        private readonly IGroupService _groupService;

        public StudentController(IUploadService<Student, StudentDto> uploadService, IGroupService groupService) : base(uploadService)
        {
            _groupService = groupService;
        }

        [HttpPost]
        [Route("api/student/upload/{groupId}")]
        public async Task<List<StudentDto>> UploadStudents(int groupId)
        {
            var students = await UploadMultiple();
            await _groupService.AddStudentsToGroup(groupId, students);
            return students;
        }
    }
}