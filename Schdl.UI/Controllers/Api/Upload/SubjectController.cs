﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Models;
using Schdl.Business.Upload;
using Schdl.Domain.Entities.Schedule;

namespace Schdl.Controllers.Api.Upload
{
    public class SubjectController: UploadController<Subject, SubjectDto>
    {
        public SubjectController(IUploadService<Subject, SubjectDto> uploadService): base(uploadService)
        {
        }

        [HttpPost]
        [Route("api/Subject/upload")]
        public async Task<List<SubjectDto>> UploadSubjects()
        {
            return await UploadMultiple();
        }
    }
}