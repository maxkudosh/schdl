﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Models;
using Schdl.Business.Upload;
using Schdl.Domain.Entities.Schedule;

namespace Schdl.Controllers.Api.Upload
{
    public class TeacherController: UploadController<Teacher, TeacherDto>
    {
        public TeacherController(IUploadService<Teacher, TeacherDto> uploadService): base(uploadService)
        {
        }

        [HttpPost]
        [Route("api/Teacher/upload")]
        public async Task<List<TeacherDto>> UploadTeachers()
        {
            return await UploadMultiple();
        }
    }
}