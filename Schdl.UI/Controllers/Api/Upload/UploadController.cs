﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Schdl.Business.Upload;
using Schdl.Domain.Entities;

namespace Schdl.Controllers.Api.Upload
{
    public abstract class UploadController<TDomain, TDto>: ApiController where TDomain : Entity
    {
        private readonly IUploadService<TDomain, TDto> _uploadService;

        protected UploadController(IUploadService<TDomain, TDto> uploadService)
        {
            _uploadService = uploadService;
        }

        protected async Task<TDto> Upload()
        {
            var provider = new MultipartMemoryStreamProvider();

            await Request.Content.ReadAsMultipartAsync(provider);

            var json = await provider.Contents.First().ReadAsStringAsync();

            return await _uploadService.Upload(json);
        }

        protected async Task<List<TDto>> UploadMultiple()
        {
            var provider = new MultipartMemoryStreamProvider();

            await Request.Content.ReadAsMultipartAsync(provider);

            var json = await provider.Contents.First().ReadAsStringAsync();

            return await _uploadService.UploadMultiple(json);
        }
    }
}