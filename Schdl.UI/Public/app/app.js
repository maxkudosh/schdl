﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, $urlRouterProvider, $httpProvider, $provide, routes) {

        const defaultRoute = routes.getDefault();
        $urlRouterProvider.otherwise(defaultRoute.path);

        $stateProvider
            .state(routes.app.stateName, {
                url: routes.app.path,
                templateUrl: '/public/app/app.html'
            });

        $provide.factory('httpInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
            return {
                'request': function (config) {
                    $rootScope.$broadcast('httpCallStarted', config);
                    return config || $q.when(config);
                },
                'response': function (response) {
                    $rootScope.$broadcast('httpCallEnded', response);
                    return response || $q.when(response);
                },
                'requestError': function (rejection) {
                    $rootScope.$broadcast('httpCallEnded', rejection);
                    return $q.reject(rejection);
                },
                'responseError': function (rejection) {
                    $rootScope.$broadcast('httpCallEnded', rejection);
                    return $q.reject(rejection);
                }
            };
        }]);
        $httpProvider.interceptors.push('httpInterceptor');
    }

    configureRoutes.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$provide', 'routes'];

    angular
        .module('schdl', [
            'ui.router',
            'schdl.routes',
            'schdl.navbar',
            'schdl.upload',
            'schdl.core'
        ])
        .config(configureRoutes);

})(angular);
