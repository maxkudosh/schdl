﻿(function (angular) {

    'use strict';

    function NavbarController(stateManager, routes) {

        this.stateManager = stateManager;

        this.menu = Object
            .keys(routes)
            .map(key => routes[key])
            .filter(route => route.navigatable);

        this.activate();
    }

    NavbarController.prototype.activate = function() {


    };

    NavbarController.prototype.isStateActive = function(name) {

        return this.stateManager.current.name.includes(name);

    };

    NavbarController.$inject = ['$state', 'routes'];

    angular
        .module('schdl.navbar')
        .controller('NavbarController', NavbarController);

})(angular);
