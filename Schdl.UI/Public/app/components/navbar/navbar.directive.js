﻿(function (angular) {

    'use strict';

    angular
        .module('schdl.navbar')
        .directive('navigation', function () {

        return {

            restrict: 'E',
            templateUrl: '/public/app/components/navbar/navbar.html'

        };

    });

})(angular);
