﻿(function (angular) {

    'use strict';

    angular
        .module('schdl.navbar', [
            'ui.bootstrap',
            'ui.router'
        ]);

})(angular);
