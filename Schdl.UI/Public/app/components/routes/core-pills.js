﻿(function (angular) {

    'use strict';

    function buildPill(stateName, name, link) {

        return {
            stateName: stateName,
            name: name,
            path: link
        };

    }

    const pills = {
        groups: buildPill('app.core.groups', 'Расписания групп', 'groups/')
    };

    angular
        .module('schdl.routes')
        .constant('corePills', pills);

})(angular);
