﻿(function (angular) {

    'use strict';

    function buildRoute(stateName, name, link) {

        return {
            stateName: stateName,
            name: name,
            path: link
        };

    }

    function buildNavbarRoute(stateName, name, link) {

        let route = buildRoute(stateName, name, link);

        route.navigatable = true;

        return route;

    }

    const routes = {
        app: buildRoute('app', 'СКДЛ', '/'),
        upload: buildNavbarRoute('app.upload', 'Данные', 'upload/'),
        core: buildNavbarRoute('app.core', 'Расписания', 'core/')
    }

    routes.getDefault = () => routes.app;

    angular
        .module('schdl.routes', [])
        .constant('routes', routes);

})(angular);
