﻿(function (angular) {

    'use strict';

    function buildPill(stateName, name, link) {

        return {
            stateName: stateName,
            name: name,
            path: link
        };

    }

    const pills = {
        groups: buildPill('app.upload.groups', 'Группы и Студенты', 'groups/'),
        facilities: buildPill('app.upload.facilities', 'Корпуса и Аудитории', 'facilities/'),
        teachers: buildPill('app.upload.teachers', 'Преподаватели ', 'teachers/'),
        subjects: buildPill('app.upload.subjects', 'Предметы', 'subjects/'),
        plans: buildPill('app.upload.plans', 'Учебные Планы', 'plans/')
    };

    angular
        .module('schdl.routes')
        .constant('uploadPills', pills);

})(angular);
