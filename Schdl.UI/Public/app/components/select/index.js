﻿(function (angular) {

    'use strict';

    function selectDirective() {

        return {

            restrict: 'E',
            templateUrl: '/public/app/components/select/index.html',
            scope: {
                onSelect: '&onselect',
                source: '=',
                text: '@',
                valueField: '@valuefield',
                displayField: '@displayfield'
            },
            link: function (scope) {

                scope.getValue = function (item) {
                    return item[scope.valueField];
                };

                scope.getDisplay = function(item) {
                    return item[scope.displayField];
                };
            }

        };

    }

    angular
        .module('schdl.select', [])
        .directive('select', selectDirective);

})(angular);
