﻿(function (angular) {

    'use strict';

// ReSharper disable once InconsistentNaming
    function Service(apiWrapper, apiUri, mapper) {

        this.api = apiWrapper(apiUri);

        this.mapper = mapper;

    }

    Service.prototype.getAll = function (params) {

        return this.api.query(params);

    };

    Service.prototype.get = function (id) {

        return this.api.get(id);

    };

    Service.prototype.update = function (data) {

        return this.api.update(data);

    };

    Service.prototype.create = function (data) {

        return this.api.save(data);

    };

    function serviceFactory(apiWrapper) {

        return function (apiUri) {

            return new Service(apiWrapper, apiUri);

        };
    }

    serviceFactory.$inject = ['$resource'];

    angular
        .module('schdl.services', ['ngResource'])
        .service('serviceFactory', serviceFactory);

})(angular);
