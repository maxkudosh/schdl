﻿(function (angular) {

    'use strict';

    function sidebarDirective() {
        
        return {

            restrict: 'E',
            templateUrl: '/public/app/components/sidebar/sidebar.html',
            scope: {
                pills: '='
            }

        };

    };

    function sidebarController(stateManager, scope) {

        this.pills = scope.pills;
        this.stateManager = stateManager;

        this.menu = Object
            .keys(this.pills)
            .map(key => this.pills[key])
            .filter(route => route.navigatable);

        this.activate();
    }

    sidebarController.prototype.activate = function () {


    };

    sidebarController.prototype.isPillActive = function (name) {

        return this.stateManager.current.name.includes(name);

    };

    sidebarController.$inject = ['$state', '$scope'];

    angular
        .module('schdl.sidebar', [
            'ui.bootstrap',
            'ui.router'
        ])
        .controller('SidebarController', sidebarController)
        .directive('sidebar', sidebarDirective);

})(angular);
