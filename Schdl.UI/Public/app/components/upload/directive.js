﻿(function (angular) {

    'use strict';

    function uploadDirective() {

        return {

            restrict: 'E',
            templateUrl: '/public/app/components/upload/upload.html',
            scope: {
                files: '='
            }

        };

    };

    angular
        .module('schdl.fileupload')
        .directive('upload', uploadDirective);

})(angular);
