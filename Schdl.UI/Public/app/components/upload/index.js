﻿(function (angular) {

    'use strict';

    function UploadService(url, upload) {

        this.upload = function (file) {

            return upload.upload({
                url: url,
                data: {
                    file: file
                }
            });
        };

    }

    function UploadServiceFactory(upload) {

        this.create = function (url) {
            return new UploadService(url, upload);
        }

    }

    UploadServiceFactory.$inject = ['Upload'];

    angular
        .module('schdl.fileupload', ['ngFileUpload'])
        .service('UploadServiceFactory', UploadServiceFactory);

})(angular, document);
