﻿(function (angular) {

    'use strict';

    function groupCoreController(serviceFactory, stateManager) {

        this.groups = serviceFactory('/api/group/').getAll();

        this.showStudents = function(group) {
            return stateManager.go('app.core.groups.students', { groupId: group.id });
        };

        this.showPlan = function (group) {
            return stateManager.go('app.core.groups.plan', { groupId: group.id });
        };

        this.showSchedule = function (group) {
            return stateManager.go('app.core.groups.schedule', { groupId: group.id });
        };

        //this.requestSchedule = function (group) {
        //    // TODO: students popup
        //};
    }

    groupCoreController.$inject = ['serviceFactory', '$state'];

    function configureRoutes($stateProvider, pills) {

        $stateProvider
            .state(pills.groups.stateName, {
                url: pills.groups.path,
                templateUrl: '/public/app/core/group/index.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'corePills'];

    angular
        .module('schdl.core.group', [
            'ui.router',
            'schdl.routes',
            'schdl.services',
            'schdl.sidebar',
            'schdl.core.group.schedule',
            'schdl.core.group.plan',
            'schdl.core.group.students'
        ])
        .config(configureRoutes)
        .controller('GroupCoreController', groupCoreController);

})(angular);
