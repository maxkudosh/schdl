﻿(function (angular) {

    'use strict';

    function planController(serviceFactory, stateManager) {

        this.groupId = stateManager.params.groupId;

        this.subjects = {};

        this.group = serviceFactory(`/api/group/single/${this.groupId}`).get().$promise.then(group => {

            var subjects = {};

            group.plan.subjects.forEach(i => {

                if (!subjects[i.subject.name]) {
                    subjects[i.subject.name] = {};
                }

                subjects[i.subject.name][i.type] = i;

            });

            this.subjects = subjects;

        });
    }

    planController.$inject = ['serviceFactory', '$state'];

    function configureRoutes($stateProvider) {

        const route = {
            stateName: 'app.core.groups.plan',
            name: 'План',
            path: ':groupId/plan/'
        };

        let popup;

        $stateProvider
            .state(route.stateName, {
                url: route.path,
                onEnter: [
                    '$uibModal', function (modal) {
                        popup = modal.open({
                            templateUrl: '/public/app/core/group/plan/index.html',
                            backdrop: 'static',
                            size: 'lg'
                        });
                    }
                ],
                onExit: function () {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider'];

    angular
        .module('schdl.core.group.plan', [
            'ui.router',
            'schdl.services'
        ])
        .config(configureRoutes)
        .controller('PlanController', planController);

})(angular);
