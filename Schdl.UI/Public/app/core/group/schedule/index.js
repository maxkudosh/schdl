﻿(function (angular) {

    'use strict';

    function scheduleController(serviceFactory, stateManager) {

        this.getType = type => {
            if (type === 'Lecture') {
                return 'Лекция';
            }
            if (type === 'Practice') {
                return 'Практика';
            }
            return 'Лабораторная';
        };

        this.getDay = day => {
            if (day === 0) {
                return 'Понедельник';
            }
            if (day === 1) {
                return 'Вторник';
            }
            if (day === 2) {
                return 'Среда';
            }
            if (day === 3) {
                return 'Четверг';
            }
            if (day === 4) {
                return 'Пятница';
            }
            return 'Суббота';
        };

        this.times = [0, 1, 2, 3, 4, 5, 6, 7];

        this.getClass = (time, day) =>  day.classes.find(c => c.time === time);

        this.groupId = stateManager.params.groupId;

        this.schedule = serviceFactory(`/api/schedule/${this.groupId}`).get();
    }

    scheduleController.$inject = ['serviceFactory', '$state'];

    function configureRoutes($stateProvider) {

        const route = {
            stateName: 'app.core.groups.schedule',
            name: 'Расписание',
            path: ':groupId/schedule/'
        };

        let popup;

        $stateProvider
            .state(route.stateName, {
                url: route.path,
                onEnter: [
                    '$uibModal', function (modal) {
                        popup = modal.open({
                            templateUrl: '/public/app/core/group/schedule/index.html',
                            backdrop: 'static',
                            size: 'lg'
                        });
                    }
                ],
                onExit: function () {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider'];

    angular
        .module('schdl.core.group.schedule', [
            'ui.router',
            'schdl.services'
        ])
        .config(configureRoutes)
        .controller('ScheduleController', scheduleController);

})(angular);
