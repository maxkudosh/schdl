﻿(function (angular) {

    'use strict';

    function studentsController(serviceFactory, stateManager) {

        this.groupId = stateManager.params.groupId;

        this.students = [];

        this.group = serviceFactory(`/api/group/single/${this.groupId}`).get().$promise.then(group => {

            this.students = group.students;

        });
    }

    studentsController.$inject = ['serviceFactory', '$state'];

    function configureRoutes($stateProvider) {

        const route = {
            stateName: 'app.core.groups.students',
            name: 'Студенты',
            path: ':groupId/students/'
        };

        let popup;

        $stateProvider
            .state(route.stateName, {
                url: route.path,
                onEnter: [
                    '$uibModal', function (modal) {
                        popup = modal.open({
                            templateUrl: '/public/app/core/group/students/index.html',
                            backdrop: 'static',
                            size: 'lg'
                        });
                    }
                ],
                onExit: function () {
                    popup.close();
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider'];

    angular
        .module('schdl.core.group.students', [
            'ui.router',
            'schdl.services'
        ])
        .config(configureRoutes)
        .controller('StudentsController', studentsController);

})(angular);
