﻿(function (angular) {

    'use strict';

    function coreController(pills) {

        this.pills = pills;

    }

    coreController.$inject = ['corePills'];

    function configureRoutes($stateProvider, routes) {

        $stateProvider
            .state(routes.core.stateName, {
                url: routes.core.path,
                templateUrl: '/public/app/core/index.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'routes'];

    angular
        .module('schdl.core', [
            'ui.router',
            'schdl.routes',
            'schdl.sidebar',
            'schdl.core.group'
        ])
        .config(configureRoutes)
        .controller('CoreController', coreController);

})(angular);
