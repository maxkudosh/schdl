﻿(function (angular) {

    'use strict';

    function facilityUploadController(scope, upload) {

        this.facilities = [];
        this.facilityUpload = upload.create('/api/facility/upload');
        scope.$watch('facilityFile', () => {

            if (!scope.facilityFile) {
                return;
            }

            this.facilityUpload.upload(scope.facilityFile).then(response => {

                this.facilities = this.facilities.concat(response.data);
                this.facilityFile = null;

            });

        });

        this.selectedFacility = null;
        this.select = facility => this.selectedFacility = facility;

        scope.$watch('upload.roomFile', () => {

            if (!scope.upload.roomFile) {
                return;
            }

            var roomUpload = upload.create(`/api/room/upload/${this.selectedFacility.id}`);
            roomUpload.upload(scope.upload.roomFile).then(response => {

                this.selectedFacility.rooms = this.selectedFacility.rooms.concat(response.data);
                this.roomFile = null;

            });

        });
    }

    facilityUploadController.$inject = ['$scope', 'UploadServiceFactory'];

    function configureRoutes($stateProvider, pills) {

        $stateProvider
            .state(pills.facilities.stateName, {
                url: pills.facilities.path,
                templateUrl: '/public/app/upload/facility/index.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'uploadPills'];

    angular
        .module('schdl.upload.facility', [
            'ui.router',
            'schdl.routes',
            'schdl.fileupload',
            'schdl.sidebar'
        ])
        .config(configureRoutes)
        .controller('FacilityUploadController', facilityUploadController);

})(angular);
