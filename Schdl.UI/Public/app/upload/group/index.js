﻿(function (angular) {

    'use strict';

    function groupUploadController(scope, upload) {

        this.group = null;
        this.groupUpload = upload.create('/api/group/upload');
        scope.$watch('groupFile', () => {

            if (!scope.groupFile) {
                return;
            }

            this.groupUpload.upload(scope.groupFile).then(response => {

                this.group = response.data;

            });

        });

        this.students = [];
        scope.$watch('studentFile', () => {

            if (!scope.studentFile) {
                return;
            }

            const studentUpload = upload.create(`/api/student/upload/${this.group.id}`);

            studentUpload.upload(scope.studentFile).then(response => {

                this.students = response.data;

            });

        });
        this.allowStudentsUpload = () => !!this.group;
    }

    groupUploadController.$inject = ['$scope', 'UploadServiceFactory'];

    function configureRoutes($stateProvider, pills) {

        $stateProvider
            .state(pills.groups.stateName, {
                url: pills.groups.path,
                templateUrl: '/public/app/upload/group/group.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'uploadPills'];

    angular
        .module('schdl.upload.group', [
            'ui.router',
            'schdl.routes',
            'schdl.fileupload',
            'schdl.sidebar'
        ])
        .config(configureRoutes)
        .controller('GroupUploadController', groupUploadController);

})(angular);
