﻿(function (angular) {

    'use strict';

    function planUploadController(scope, upload, serviceFactory) {

        this.groupService = serviceFactory('/api/group/noplan');
        this.groups = this.groupService.getAll();

        this.selectedGroupId = null;
        this.onGroupSelected = () => {
            debugger;
            this.group = this.groups.find(g => g.id === +this.selectedGroupId);
        }

        this.plan = null;
        scope.$watch('planFile', () => {

            if (!scope.planFile) {
                return;
            }

            upload
                .create(`/api/plan/upload/${this.selectedGroupId}`)
                .upload(scope.planFile).then(response => {

                    this.plan = response.data;
                    this.planFile = null;

                });

        });
    }

    planUploadController.$inject = ['$scope', 'UploadServiceFactory', 'serviceFactory'];

    function configureRoutes($stateProvider, pills) {

        $stateProvider
            .state(pills.plans.stateName, {
                url: pills.plans.path,
                templateUrl: '/public/app/upload/plan/index.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'uploadPills'];

    angular
        .module('schdl.upload.plan', [
            'ui.router',
            'schdl.routes',
            'schdl.fileupload',
            'schdl.sidebar',
            'schdl.services'
        ])
        .config(configureRoutes)
        .controller('PlanUploadController', planUploadController);

})(angular);
