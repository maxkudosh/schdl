﻿(function (angular) {

    'use strict';

    function subjectUploadController(scope, upload) {

        this.subjects = [];

        this.subjectUpload = upload.create('/api/subject/upload');

        scope.$watch('subjectFile', () => {

            if (!scope.subjectFile) {
                return;
            }

            this.subjectUpload.upload(scope.subjectFile).then(response => {

                this.subjects = this.subjects.concat(response.data);
                this.subjectFile = null;

            });

        });
    }

    subjectUploadController.$inject = ['$scope', 'UploadServiceFactory'];

    function configureRoutes($stateProvider, pills) {

        $stateProvider
            .state(pills.subjects.stateName, {
                url: pills.subjects.path,
                templateUrl: '/public/app/upload/subject/subject.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'uploadPills'];

    angular
        .module('schdl.upload.subject', [
            'ui.router',
            'schdl.routes',
            'schdl.fileupload',
            'schdl.sidebar'
        ])
        .config(configureRoutes)
        .controller('SubjectUploadController', subjectUploadController);

})(angular);
