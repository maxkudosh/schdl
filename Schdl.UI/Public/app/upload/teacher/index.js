﻿(function (angular) {

    'use strict';

    function teacherUploadController(scope, upload) {

        this.teachers = [];

        this.teacherUpload = upload.create('/api/teacher/upload');

        scope.$watch('teacherFile', () => {

            if (!scope.teacherFile) {
                return;
            }

            this.teacherUpload.upload(scope.teacherFile).then(response => {

                this.teachers = this.teachers.concat(response.data);
                this.teacherFile = null;

            });

        });
    }

    teacherUploadController.$inject = ['$scope', 'UploadServiceFactory'];

    function configureRoutes($stateProvider, pills) {

        $stateProvider
            .state(pills.teachers.stateName, {
                url: pills.teachers.path,
                templateUrl: '/public/app/upload/teacher/index.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'uploadPills'];

    angular
        .module('schdl.upload.teacher', [
            'ui.router',
            'schdl.routes',
            'schdl.fileupload',
            'schdl.sidebar'
        ])
        .config(configureRoutes)
        .controller('TeacherUploadController', teacherUploadController);

})(angular);
