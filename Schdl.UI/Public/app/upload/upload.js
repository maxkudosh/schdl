﻿(function (angular) {

    'use strict';

    function uploadController(pills) {

        this.pills = pills;

    }

    uploadController.$inject = ['uploadPills'];

    function configureRoutes($stateProvider, routes) {

        $stateProvider
            .state(routes.upload.stateName, {
                url: routes.upload.path,
                templateUrl: '/public/app/upload/upload.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'routes'];

    angular
        .module('schdl.upload', [
            'ui.router',
            'schdl.routes',
            'schdl.sidebar',
            'schdl.upload.group',
            'schdl.upload.subject',
            'schdl.upload.teacher',
            'schdl.upload.facility',
            'schdl.upload.plan'
        ])
        .config(configureRoutes)
        .controller('UploadController', uploadController);

})(angular);
